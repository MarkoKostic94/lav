export const config = {
    appName: 'My awesome ZOO! :)',
    apiUrl: 'http://localhost:3000',
    lavoviTableConfig: [
        { key: 'id', label: 'Id'},
        { key: 'ime', label: 'Ime'},
        { key: 'kilaza', label: 'Kilaza'},
        { key: 'datumRodjenja', label: 'Datum Rodjenja'}
    ]
}