import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.route.html'
})
export class HomeComponent implements OnInit {

  @ViewChild("title") elementTajtla: ElementRef;

  constructor() { }

  ngOnInit() {
    this.elementTajtla.nativeElement.style="color:red"
  }

}
