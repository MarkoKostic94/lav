import { Component, OnInit } from '@angular/core';
import { LavoviModel } from '../../models/lavovi.model';
import { config } from '../../config';
import { Router } from '@angular/router';

@Component({
  selector: 'table-lava',
  templateUrl: './table-lava.route.html'
})
export class TableLavaComponent implements OnInit {

  searchString = "";
  conf = config.lavoviTableConfig;

  constructor(public model:LavoviModel, private router:Router) { }

  ngOnInit() {
  }

  obrisiLava(id) {
    this.model.obrisiLava(id);
  }

  editujLava(id) {
    this.router.navigate(['/lav',id,'edit']);
  }

}
