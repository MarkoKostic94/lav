import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lav',
  templateUrl: './lav.route.html'
})
export class LavComponent implements OnInit {

  id;

  constructor(private router:Router, private route:ActivatedRoute) {
    this.route.params.subscribe(({id})=>{
      this.id = id;
    })
   }

   goToPath(subpath) {
     this.router.navigate(['/pingvin', this.id, subpath])
   }

  ngOnInit() {
  }

}
