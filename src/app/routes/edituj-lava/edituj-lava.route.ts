import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LavoviModel } from '../../models/lavovi.model';

@Component({
  selector: 'edituj-lava',
  templateUrl: './edituj-lava.route.html'
})
export class EditujLavaComponent implements OnInit {

  editovaniLav = {};

  constructor(private route:ActivatedRoute, public model:LavoviModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getLavaById(id,(lav)=>{
          this.editovaniLav = lav;
        });
      }
    );
   }

   updejtujLava() {
     this.model.updejtujLava(this.editovaniLav);
   }

  ngOnInit() {
  }

}
