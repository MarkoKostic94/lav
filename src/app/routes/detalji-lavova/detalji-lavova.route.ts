import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LavoviModel } from '../../models/lavovi.model';

@Component({
  selector: 'detalji-lavova',
  templateUrl: './detalji-lavova.route.html'
})
export class DetaljiLavovaComponent implements OnInit {

  lav = {};

  constructor(private route:ActivatedRoute, public model:LavoviModel) { 
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getLavaById(id, (lav)=>{
          this.lav = lav;
        });
      }
    );
  }

  ngOnInit() {
  }

}
