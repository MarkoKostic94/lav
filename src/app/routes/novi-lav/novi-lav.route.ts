import { Component, OnInit } from '@angular/core';
import { LavoviModel } from '../../models/lavovi.model';

@Component({
  selector: 'novi-lav',
  templateUrl: './novi-lav.route.html'
})
export class NoviLavComponent implements OnInit {

  noviLav = {
    ime:null,
    kilaza:null,
    datumRodjenja:null
  };

  constructor(public model:LavoviModel) { }

  dodajLava() {
    if(this.noviLav.ime && this.noviLav.kilaza && this.noviLav.datumRodjenja) {
      this.model.dodajLava(this.noviLav);
    }
    console.log(this.noviLav)
  }

  ngOnInit() {
  }

}
