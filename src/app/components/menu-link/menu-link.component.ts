import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'menu-link',
    templateUrl: './menu-link.component.html'
})

export class MenuLinkComponent implements OnInit {

    @Input('vrednost') link;

    @Output('onLinkClicked') kadSeKlikneLink = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {

    }

    linkKliknut() {
        this.kadSeKlikneLink.emit();
    }
}