import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { config } from '../config';
import "rxjs/add/operator/map";


@Injectable({
    providedIn: 'root'
   })
   export class LavoviService {
   
    constructor(private http:Http) {
      
    }
   
    getAll(){
      return this.http
            .get(config.apiUrl + '/lavovi')
            .map((response) => { return response.json() });
   }

   getById(id) {
    return this.http
            .get(config.apiUrl + '/lavovi/' + id)
            .map((response) => { return response.json() });
   }

   post(lav) {
    return this.http
            .post(config.apiUrl + '/lavovi', lav)
            .map((response) => { return response.json() });
   }

   update(lav) {
    return this.http
            .patch(config.apiUrl + '/lavovi/' + lav.id, lav)
            .map((response) => { return response.json() });
   }

   delete(id) {
    return this.http
            .delete(config.apiUrl + '/lavovi/' + id)
            .map((response) => { return response.json() });
   }
   
}