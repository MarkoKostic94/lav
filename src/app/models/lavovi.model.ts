import { Injectable } from '@angular/core';
import { LavoviService } from '../services/lavovi.service';
import { Router } from '@angular/router';

@Injectable()
export class LavoviModel {

    lavovi = [];

    constructor(private service:LavoviService, private router: Router) {
        this.refreshujLavove(()=>{});
    }

    refreshujLavove(clbk) {
        this.service.getAll().subscribe(
            (lavovi) => {
                this.lavovi = lavovi;
                clbk()
            }
        );
    }

    updejtujLava(lav) {
        this.service.update(lav).subscribe((updejtovaniLav)=> {
            this.refreshujLavove(()=>{})
        })
        this.router.navigate(['/dashboard']);
    }

    getLavaById(id, clbk) {
        this.service.getById(id).subscribe(clbk);
    }

    getLavaByIdCallback(id, clbk) {
        if(this.lavovi.length < 1) {
            this.refreshujLavove(()=>{
                for(let i = 0; i < this.lavovi.length; i++) {
                    if(id == this.lavovi[i].id) {
                        clbk(this.lavovi[i])
                    }
                }
            })
        } else {
            for(let i = 0; i < this.lavovi.length; i++) {
                if(id == this.lavovi[i].id) {
                    clbk(this.lavovi[i])
                }
            }
        }
    }

    dodajLava(lav) {
        this.service.post(lav).subscribe((lav) => {
            this.lavovi.push(lav);
            this.router.navigate(['/dashboard']);
        })
    }

    dajMiIndexLavaPoIdju(id,clbk) {
        for (let i = 0; i < this.lavovi.length; i++) {
            if (id == this.lavovi[i].id) {
                clbk(i)
            }
        }
    }

    obrisiLava(id) {
        this.service.delete(id).subscribe(() => {
            this.dajMiIndexLavaPoIdju(id,(index)=>{
                 this.lavovi.splice(index,1);
             });
             this.refreshujLavove(id);
            
         });
    }
}