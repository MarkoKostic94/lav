import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPoStringu'
})
export class FilterPipe implements PipeTransform {

  transform(values, filterString) {
    return values.filter((lav)=> {
      return (lav.ime.toLowerCase().indexOf(filterString.toLowerCase()) > -1);
    });
  }

}
