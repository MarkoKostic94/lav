import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


import { LavoviComponent } from './lavovi.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';

import { MenuModel } from './models/menu.model';

import { HomeComponent } from './routes/home/home.route';
import { TableLavaComponent } from './routes/table-lava/table-lava.route';
import { NoviLavComponent } from './routes/novi-lav/novi-lav.route';
import { LavComponent } from './routes/lav/lav.route';
import { EditujLavaComponent } from './routes/edituj-lava/edituj-lava.route';
import { DetaljiLavovaComponent } from './routes/detalji-lavova/detalji-lavova.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { LavoviModel } from './models/lavovi.model';
import { LavoviService } from './services/lavovi.service';
import { FilterPipe } from './pipes/filter.pipe';

const routes:Routes = [
  {path:"", component:HomeComponent},
  {path:"dashboard", component:TableLavaComponent},
  {path:"lavovi/novi", component:NoviLavComponent},
  {path:"lav/:id", component:LavComponent},
  {path:"lav/:id/edit", component:EditujLavaComponent},
  {path:"lav/:id/info", component:DetaljiLavovaComponent},
]

@NgModule({
  declarations: [
    LavoviComponent,
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    DetaljiLavovaComponent,
    EditujLavaComponent,
    HomeComponent,
    MenuLinkComponent,
    NoviLavComponent,
    LavComponent,
    TableLavaComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    MenuModel,
    LavoviModel,
    LavoviService
  ],
  bootstrap: [LavoviComponent]
})
export class LavoviModule { }
